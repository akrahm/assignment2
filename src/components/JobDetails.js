import React from "react";

class JobDetails extends React.Component {
  render() {
    return (
      <div style={{textAlign:"center", border: "1px solid black"}}>
        <h4>{this.props.job?.name}</h4>
        <h5>
          {this.props.job?.location?.city},{this.props.job?.location?.country}
        </h5>
        <img alt="logo" src={this.props.job?.logo} className="job-post-image" />
        <p>{this.props.job?.description}</p>
        <div className="salary"><b>{this.props.job?.salaryInString}</b></div>

        <hr />
      </div>
    );
  }
}

export default JobDetails;
