import React from "react";

class SearchBarForLocation extends React.Component {
  state = { term: "" };

  onFormSubmit = (event) => {
    event.preventDefault();
    let filterList = [];
    if (this.state.term) {
      filterList = this.props.jobs.filter((f) => {
        if (
          f.location.city.toString().toLowerCase() ===
          this.state.term.toString().toLowerCase()
        ) {
          return true;
        } else {
          return false;
        }
      });
    } else {
      filterList = this.props.jobs;
    }

    this.props.onSearchBarForLocation(filterList);
  };

  render() {
    return (
      <div className="ui segment">
        <form onSubmit={this.onFormSubmit} className="ui form">
          <div className="field">
            <label>Where:</label>
            <input
              type="text"
              value={this.state.term}
              onChange={(e) => this.setState({ term: e.target.value })}
              placeholder="Enter City"
            />
          </div>
        </form>
      </div>
    );
  }
}

export default SearchBarForLocation;
