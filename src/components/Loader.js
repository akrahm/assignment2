import React from "react";

export class Loader extends React.Component {
  render() {
    return (
      <div>
        <h5>Loading..</h5>
      </div>
    );
  }
}

export default Loader;
