import React from "react";
import Loader from "./Loader";

export class JobBrief extends React.Component {
  state = {
    name: "",
    location: "",
    logo: "",
    description: "",
    salary: "",
    city: "",
    loading: false,
  };

  componentDidMount() {
    let { name, location, logo, description, salaryInString } = this.props;
    this.setState({
      name,
      location,
      logo,
      description,
      salaryInString,
    });
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ loading: false });
    }, 5000);
  }
  onClickJob = (event) => {
    this.props.onClick(this.state);
  };
  render() {
    return (
      <>
        {this.state.loading && <Loader />}
        {!this.state.loading && (
          <div
            style={{ width: "50vw", textAlign: "left", padding: "5px" }}
            className="job-brief"
            onClick={this.onClickJob}
          >
            <h4>{this.props.name}</h4>
            <h5>
              {this.props.location?.city},{this.props.location?.country}
            </h5>
            <img alt="logo" src={this.props.logo} className="job-post-image" />
            <p>{this.props.description}</p>
            <div className="salary"><b>Salary</b>: {this.props.salaryInString}</div>

       
            <div className="btns">
            <button className="btn_Apply"><b>Apply</b></button>
                  <button className="btn_Reject"><b>Reject</b></button>
        </div>
        <hr />
          </div>
          
        )}
      </>
    );
  }
}

export default JobBrief;
