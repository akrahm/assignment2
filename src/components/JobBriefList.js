import React from "react";
import JobBrief from "./JobBrief";
import JobDetails from "./JobDetails";
import Loader from "./Loader";

 class JobBriefList extends React.Component {
  constructor(props) {
    super(props);

    this.state = { selectedJob: null, loading: false, jobs: [] };
  }
  onJobClick = (e) => {
    this.setState({ selectedJob: e });
  };

  componentDidMount() {
    this.setState({
      loading: true,
      jobs: this.props.jobList,
      selectedJob: null,
    });

    setTimeout(() => {
      this.setState({ loading: false });
    }, 5000);
  }

  render() {
    return (
      <div
        style={{
          display: "flex",
          alignItems: "flex-start",
          marginTop: "5px",
          overflow: "hidden",
        }}
      >
        <div
          style={{
            margin: "auto",
            width: "40vw",
            border: "1px solid gray",
            maxHeight: "93vh",
            overflow: "auto",
            borderRadius: "4px",
          }}
        >
          {this.state.loading && <Loader />}
          {!this.state.loading &&
            this.props.jobList.map((job, index) => (
              <JobBrief
                key={index}
                name={job.name}
                salary={job.salary}
                salaryInString={intToString(job.salary)}
                description={job.description}
                logo={job.logo}
                location={job.location}
                onClick={this.onJobClick}
              />
            ))}
        </div>

        <div style={{ width: "100%", margin: "0 auto" }}>
          {this.state.selectedJob && (
            <JobDetails job={this.state.selectedJob} />
          )}
        </div>
      </div>
    );
  }
}

function intToString(value) {
  var suffixes = ["", "k", "m", "b", "t"];
  var suffixNum = Math.floor(("" + value).length / 3);
  var shortValue = parseFloat(
    (suffixNum !== 0 ? value / Math.pow(1000, suffixNum) : value).toPrecision(2)
  );
  if (shortValue % 1 !== 0) {
    shortValue = shortValue.toFixed(1);
  }
  return shortValue + suffixes[suffixNum];
}


export default JobBriefList;
