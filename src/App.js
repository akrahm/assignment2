
import React from "react";
import JobBriefList from "./components/JobBriefList";
import SearchBarForLocation from "./components/SearchBarForLocation";
import SearchBarForName from "./components/SearchBarForName";
import Jobs from "./jobs.json";
import "./App.css";
import Demo from './components/Demo';

class App extends React.Component {
  state = { filterJobs: Jobs, jobs: Jobs };
  filterByName = (e) => {
    console.log("filterbyname", e);
    this.setState({ filterJobs: e });
  };
  filterByLocation = (e) => {
    console.log("filter by locaiton", e);
    this.setState({ filterJobs: e });
  };
  render() {
    return (
      <div>
        <Demo/>
        <div/>
        
            <div className="App" style={{ padding: "10px" }}>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            border: "string" ,
            backgroundColor: "grey",
          }}
        >
          <SearchBarForName
            jobs={Jobs}
            onSearchBarForName={this.filterByName}
          />
          <SearchBarForLocation
            jobs={Jobs}
            onSearchBarForLocation={this.filterByLocation}
          />
        </div>

        <JobBriefList jobList={this.state.filterJobs} />
      </div>

        </div>
      
    );
  }
}

export default App;
