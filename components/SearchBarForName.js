
import React from "react";

class SearchBarForName extends React.Component {
  constructor(props) {
    super(props);
    this.textInput = React.createRef();
    this.state = { term: "" };
  }

  componentDidMount() {
    this.textInput.current.focus();
  }

  onFormSubmit = (event) => {
    event.preventDefault();
    let filterList = [];
    if (this.state.term) {
      filterList = this.props.jobs.filter((f) => {
        if (
          f.name.toString().toLowerCase() ===
          this.state.term.toString().toLowerCase()
        ) {
          return true;
        } else {
          return false;
        }
      });
    } else {
      filterList = this.props.jobs;
    }
    this.props.onSearchBarForName(filterList);
  };

  render() {
    return (
      <div className="ui segment">
        <form onSubmit={this.onFormSubmit} className="ui form">
          <div className="field">
            <label>What:</label>
            <input
              ref={this.textInput}
              type="text"
              value={this.state.term}
              onChange={(e) => this.setState({ term: e.target.value })}
              placeholder="Job title,keywords or company"
            />
          </div>
        </form>
      </div>
    );
  }
}

export default SearchBarForName;
